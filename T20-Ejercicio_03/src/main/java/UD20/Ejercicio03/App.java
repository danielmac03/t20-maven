package UD20.Ejercicio03;

import java.awt.EventQueue;

import views.InterfazGrafica;
/**
 * Iniciamos la aplicación.
 */
public class App 
{
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazGrafica window = new InterfazGrafica();
					window.frmVentanaConMs.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
