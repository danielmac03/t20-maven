package views;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazGrafica {

	public JFrame frmVentanaConMs;
	/**
	 * Creamos la aplicación.
	 */
	public InterfazGrafica() {
		frmVentanaConMs = new JFrame();
		frmVentanaConMs.setTitle("Ventana con más interacción");
		frmVentanaConMs.setBounds(100, 100, 371, 211);
		frmVentanaConMs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmVentanaConMs.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Boton 1:");
		lblNewLabel.setBounds(43, 48, 130, 16);
		frmVentanaConMs.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Boton 2:");
		lblNewLabel_1.setBounds(207, 48, 123, 16);
		frmVentanaConMs.getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Boton 1");
		btnNewButton.addActionListener(new ActionListener() {
			int numClic = 0;
			@Override
			public void actionPerformed(ActionEvent arg0) {
				numClic = numClic + 1;
				String text = "Boton 1: " + numClic + " veces";
				lblNewLabel.setText(text);
				
			}
		});
		btnNewButton.setBounds(50, 74, 82, 25);
		frmVentanaConMs.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Boton 2");
		btnNewButton_1.addActionListener(new ActionListener() {
			int numClic1 = 0;
			@Override
			public void actionPerformed(ActionEvent e) {
				numClic1 = numClic1 + 1;
				String text1 = "Boton 2: " + numClic1 + " veces";
				lblNewLabel_1.setText(text1);
			}
		});
		btnNewButton_1.setBounds(214, 74, 82, 25);
		frmVentanaConMs.getContentPane().add(btnNewButton_1);
	}
}
