package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class InterfazGrafica {

	public JFrame frmIndiceDeMasa;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;


	/**
	 * Creamos la aplicación.
	 */
	
	public InterfazGrafica() {
		
		frmIndiceDeMasa = new JFrame();
		frmIndiceDeMasa.setTitle("Indice de Masa Corporal");
		frmIndiceDeMasa.setBounds(100, 100, 450, 173);
		frmIndiceDeMasa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIndiceDeMasa.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Altura (metros):");
		lblNewLabel.setBounds(34, 37, 100, 16);
		frmIndiceDeMasa.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Peso (kg):");
		lblNewLabel_1.setBounds(239, 37, 70, 16);
		frmIndiceDeMasa.getContentPane().add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(134, 35, 80, 19);
		frmIndiceDeMasa.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(307, 35, 80, 19);
		frmIndiceDeMasa.getContentPane().add(textField_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("IMC:");
		lblNewLabel_1_1.setBounds(227, 77, 37, 16);
		frmIndiceDeMasa.getContentPane().add(lblNewLabel_1_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(276, 75, 61, 19);
		frmIndiceDeMasa.getContentPane().add(textField_2);
		
		JButton btnNewButton = new JButton("Calcular IMC");
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				DecimalFormat decimal = new DecimalFormat("0.00");
				String texto = textField.getText();
				double altura = Double.valueOf(texto);
				String texto1 = textField_1.getText();
				double peso = Double.valueOf(texto1);
				double IMC = peso/(altura*altura);
				String texto2 = decimal.format(IMC);
				textField_2.setText(texto2);
			}
		});
		btnNewButton.setBounds(80, 73, 119, 25);
		frmIndiceDeMasa.getContentPane().add(btnNewButton);
		
		
		
		
	}
}
