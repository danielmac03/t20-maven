package UD20.Ejercicio06;

import java.awt.EventQueue;

import views.InterfazGrafica;


public class App {	
	/**
	 * Iniciamos la aplicación
	 *
	 */
    public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazGrafica window = new InterfazGrafica();
					window.frmIndiceDeMasa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    
}
