package T20.MAVEN.dto;

import javax.swing.*;
import java.awt.event.*;

public class MainViews extends JFrame{
	
	private JPanel contentPane;
	
	public MainViews() {
		setTitle("Ejercicio 02");
		setBounds(400, 200, 500, 100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel label = new JLabel("Has pulsado: ");
		label.setBounds(60, 20, 150, 20);
		contentPane.add(label);
		
		JButton btns[] = new JButton[2];
		
		btns[0] = new JButton("Boton 1");
		btns[0].setBounds(230, 20, 80, 20);
		contentPane.add(btns[0]);
		
		btns[1] = new JButton("Boton 2");
		btns[1].setBounds(330, 20, 80, 20);
		contentPane.add(btns[1]);
		
		ActionListener alBtns0 = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				label.setText("Has pulsado: Boton 1");
			}
		};

		btns[0].addActionListener(alBtns0);

		ActionListener alBtns1 = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				label.setText("Has pulsado: Boton 2");
			}
		};
		
		btns[1].addActionListener(alBtns1);
	}

}
