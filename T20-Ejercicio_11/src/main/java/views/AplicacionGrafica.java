package views;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

public class AplicacionGrafica extends JFrame {

	private JPanel contentPane;
	private JButton botones[] = new JButton[2];
	private JButton alfabeto[] = new JButton[27];
	private ArrayList<JButton> letrasPalabraButton = new ArrayList<JButton>();
	private ArrayList<Character> letrasPalabra = new ArrayList<Character>();
	private JButton pistas[] = new JButton[5];
	private JPanel paneles[] = new JPanel[3];
	private JLabel imagenes[] = new JLabel[11];
	private String palabra = null;
	private boolean noJuegar = true;
	private boolean fin = false;
	private int errores=0;
	
	String[] palabras =  new String[] { "HOLA", "ADIOS", "ANIMALES", "MAÑANA", "MOSAICO", "OPERACION", "VISTAS", "MUEBLE", "CANGREJO", "ACEITUNA" };
	
	public AplicacionGrafica() {
		setTitle("Ahorcado");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 455);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//boton inicio
		botones[0] = new JButton("Iniciar Juego");
		botones[0].setBounds(10, 24, 237, 42);
		contentPane.add(botones[0]);
		
		//boton resolver
		botones[1] = new JButton("Resolver");
		botones[1].setBounds(10, 88, 237, 42);
		contentPane.add(botones[1]);
		
		// panel para palabraAdivinar
		paneles[0] = new JPanel(); 
		paneles[0].setBounds(10, 190, 400, 64);
		contentPane.add(paneles[0]);
		
		// panel para letras
		paneles[1] = new JPanel(); 
		paneles[1].setBounds(10, 265, 400, 219);
		contentPane.add(paneles[1]);

		//panel para imegenes
		paneles[2] = new JPanel();
		paneles[2].setBackground(Color.WHITE);
		paneles[2].setBounds(470, 24, 290, 365); 
		paneles[2].setVisible(false);
		contentPane.add(paneles[2]);
		
		//botones para letras
		char cletraA = (char) 65; //codigo letra A mayúscula para empezar
		
		//bucle para hacer el teclado
		for(int i=0; i<=26; i++) {
			String arrobaComoCadena = Character.toString((char) cletraA);
			
			if (i == 14) { 
				//para poner la letra Ñ ya que al ser en ascii el codigo 
				// 165 no lo puedo poner en bucle y me da otro caracter.
				alfabeto[i] = new JButton("\u00d1"); 
			}else {
				alfabeto[i] = new JButton(arrobaComoCadena);
				cletraA++;
			}

			paneles[1].add(alfabeto[i]); 	
			
		}
		
		//ruta e variable imagenIcon 
		ImageIcon icono = new ImageIcon(AplicacionGrafica.class.getResource("/views/ahorcadoimg/bombilla.png"));
		

		//variable de distancia x
		int a = 29;
		
		//bucle para crear los 5 botones de pistas
		for (int i = 0; i <= 4; i++) {
			pistas[i] = new JButton();
			pistas[i].setIcon(icono);
			pistas[i].setBounds(a, 157, 23, 23);
			contentPane.add(pistas[i]);
			a = a + 40;
		}
		
		//imagenes ahorcado
		for (int i = 1; i <= 10; i++) {			
			//ruta imagen ahorcado
			ImageIcon imagen = new ImageIcon(AplicacionGrafica.class.getResource("/views/ahorcadoimg/" + i + ".jpg"));
			imagenes[i] = new JLabel(); 
			imagenes[i].setIcon(imagen);
			imagenes[i].setBounds(497, 11, 286, 473);
			imagenes[i].setVisible(false);
			paneles[2].add(imagenes[i]);
		}
		
		
		//primero elejir una palabra al azar:
		Random random = new Random(); 
        int numero = random.nextInt(palabras.length); 
        palabra = palabras[numero];
		
        //Generar Grid
		paneles[0].setLayout(new GridLayout(0, palabra.length(), 0, 0));
                
		//dividir esa palabra:
		for(int i = 0; i < palabra.length(); i++) {
			letrasPalabra.add(palabra.charAt(i));
			letrasPalabraButton.add(new JButton("__"));
			paneles[0].add(letrasPalabraButton.get(i));
		}
		
		ActionListener alEmpezar = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				noJuegar = false;
				botones[0].setEnabled(false);
			}
		};	
		botones[0].addActionListener(alEmpezar);
		
		
		ActionListener alResolver = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resolver();
				botones[0].setEnabled(false);
			}
		};
		botones[1].addActionListener(alResolver);
		
		
		ActionListener alAlfabeto = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int contador = 0;
				
				if (noJuegar == false) {
					JButton tmpButton = (JButton) e.getSource();
					tmpButton.setEnabled(false);
					
					for (int i = 0; i < palabra.length(); i++) {
						if (tmpButton.getText().equals(Character.toString(letrasPalabra.get(i)))) {
							letrasPalabraButton.get(i).setText(Character.toString(letrasPalabra.get(i)));
							comprobarGanar();
						}else {
							contador++;
						}
					}	
				}else {
					if (fin) {
						JOptionPane.showMessageDialog(null, "FIN DEL JUEGO");
					}else {
						JOptionPane.showMessageDialog(null, "Debe iniciar el juego primero");
					}
				}
				
				if (contador == palabra.length()) {
					
					if (errores != 0) {
						imagenes[errores].setVisible(false);
					}
					else {
						paneles[2].setVisible(true);
					}
					
					errores++;
						
						imagenes[errores].setVisible(true);
						
					comprobarPerder();
				}
			}
		};
		for (int i = 0; i < alfabeto.length; i++) {
			alfabeto[i].addActionListener(alAlfabeto);
		}
		
		
		ActionListener alPistas = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (noJuegar == false) {
					JButton tmpBtn = (JButton) e.getSource();
					tmpBtn.setEnabled(false);
					

					for (int i = 0; i < palabra.length(); i++) {
						if (letrasPalabraButton.get(i).getText().equals("__")) {
							letrasPalabraButton.get(i).setText(Character.toString(letrasPalabra.get(i)));
							break;
						}
					}	
					
					comprobarGanar();
				}else {
					if (fin) {
						JOptionPane.showMessageDialog(null, "FIN DEL JUEGO");
					}else {
						JOptionPane.showMessageDialog(null, "Debe iniciar el juego primero");
					}
				}
				
			}
		};
		for (int i = 0; i <= 4; i ++){
			pistas[i].addActionListener(alPistas);
		}
		
	}
	
	
	
	
	public void comprobarGanar() {
		int contador = 0;

		for (int i = 0; i < palabra.length(); i++) {
			if (letrasPalabraButton.get(i).getText().equals("__") == false) {
				contador++;
			}
		}
				
		if (contador == palabra.length()) {
			JOptionPane.showMessageDialog(null, "Has acertado");
			noJuegar = true;
			fin = true;
			botones[1].setEnabled(false);
		}
	}
	
	public void comprobarPerder() {
		if(errores == 10) {
			JOptionPane.showMessageDialog(null, "Has perdido");
			noJuegar = true;
			fin = true;
			resolver();
			botones[1].setEnabled(false);
		}
	}
	
	public void resolver() {
		for (int i = 0; i < palabra.length(); i++) {
			letrasPalabraButton.get(i).setText(Character.toString(letrasPalabra.get(i)));
		}
	}
}
