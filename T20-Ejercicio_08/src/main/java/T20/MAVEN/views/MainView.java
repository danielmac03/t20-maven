package T20.MAVEN.views;

import javax.swing.*;
import java.awt.event.*;

public class MainView extends JFrame {

	private JPanel contentPane;

	public MainView() {
		setTitle("Ejercicio 08");
		setBounds(400, 200, 550, 180);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
			
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel labels[] = new JLabel[3];
		JTextField textFields[] = new JTextField[2];
		
		labels[0] = new JLabel("Cantidad a convertir:");
		labels[0].setBounds(50, 25, 150, 20);
		contentPane.add(labels[0]);
		
		textFields[0] = new JTextField();
		textFields[0].setBounds(180, 25, 100, 20);
		contentPane.add(textFields[0]);
		
		labels[1] = new JLabel("Resultado:");
		labels[1].setBounds(300, 25, 100, 20);
		contentPane.add(labels[1]);
		
		textFields[1] = new JTextField();
		textFields[1].setBounds(380, 25, 90, 20);
		textFields[1].setEnabled(false);
		contentPane.add(textFields[1]);
		
		JButton btns[] = new JButton[3];
		
		btns[0] = new JButton("Pesetas a euros");
		btns[0].setBounds(65, 60, 150, 25);
		contentPane.add(btns[0]);
		
		btns[1] = new JButton("Euros a pesetas");
		btns[1].setBounds(215, 60, 150, 25);
		contentPane.add(btns[1]);
		
		btns[2] = new JButton("Borrar");
		btns[2].setBounds(365, 60, 100, 25);
		contentPane.add(btns[2]);
		
		labels[2] = new JLabel("");
		labels[2].setBounds(200, 100, 150, 20);
		contentPane.add(labels[2]);
		
		ActionListener alPtasEuros = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String cantidadConvertir = textFields[0].getText();
					double cantidadConvertirDouble = Double.parseDouble(cantidadConvertir);
					double cantidadPesetas = Math.floor((cantidadConvertirDouble / 166.386) * 100) / 100;
					String cantidadPesetasString = Double.toString(cantidadPesetas);
					textFields[1].setText(cantidadPesetasString);
					labels[2].setText("PESETAS A EUROS");
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "Debe introducir un numero");
					textFields[0].setText("");
					textFields[1].setText("");
					labels[2].setText("");
				}
			}
		};
		
		btns[0].addActionListener(alPtasEuros);
		
		ActionListener alEurosPesetas = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String cantidadConvertir = textFields[0].getText();
					double cantidadConvertirDouble = Double.parseDouble(cantidadConvertir);
					double cantidadEuros = Math.floor((cantidadConvertirDouble * 166.386) * 100) / 100;
					String cantidadEurosString = Double.toString(cantidadEuros);
					textFields[1].setText(cantidadEurosString);
					labels[2].setText("EUROS A PESETAS");
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "Debe introducir un numero");
					textFields[0].setText("");
					textFields[1].setText("");
					labels[2].setText("");
				}
			}
		};
		
		btns[1].addActionListener(alEurosPesetas);
		
		ActionListener alBorrar = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textFields[0].setText("");
				textFields[1].setText("");
				labels[2].setText("");
			}
		};
		
		btns[2].addActionListener(alBorrar);
		
		KeyListener kl = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				//El keycode 127 es el SUPRIMIR
				if(e.getKeyCode() == 127) {
					textFields[0].setText("");
					textFields[1].setText("");
					labels[2].setText("");
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		};
		
		textFields[0].addKeyListener(kl);

	}
}
