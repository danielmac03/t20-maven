package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;

public class AplicacionGrafica extends JFrame{

	private JPanel contentPane;

	public AplicacionGrafica() {
				
		setTitle("Ejercicio 1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar barraMenu = new JMenuBar();
		setJMenuBar(barraMenu);
		
		JMenu menuarchivo = new JMenu("Archivo");
		barraMenu.add(menuarchivo);
		
		JMenuItem mrestaurar = new JMenuItem("Restaurar");
		menuarchivo.add(mrestaurar);
		
		JMenuItem mmaximizar = new JMenuItem("Maximizar");
		menuarchivo.add(mmaximizar);
		
		JMenuItem mcerrar = new JMenuItem("Cerrar");
		menuarchivo.add(mcerrar);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JLabel lblNewLabel = new JLabel("Hola");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBackground(new Color(51, 0, 255));
		lblNewLabel.setBounds(76, 23, 31, 23);
		contentPane.add(lblNewLabel);
		
		JButton bpequeno = new JButton("Pequeño");
		bpequeno.setBounds(101, 176, 89, 23);
		contentPane.add(bpequeno);
		
		JButton bgrande = new JButton("Grande");
		bgrande.setBounds(206, 176, 89, 23);
		contentPane.add(bgrande);
		

		ActionListener a1 = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	dispose();
            }
        };
        ActionListener a2 = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	setExtendedState(MAXIMIZED_BOTH);
            }
        };
        
        ActionListener a3 = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	setExtendedState(NORMAL);
            }
        };
        
        ActionListener a4 = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {	
            	lblNewLabel.setSize(lblNewLabel.getWidth() / 2, lblNewLabel.getHeight() / 2);
            	lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, lblNewLabel.getFont().getSize() / 2));
            	
            }
        };
        
        ActionListener a5 = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	lblNewLabel.setSize(lblNewLabel.getWidth() * 2, lblNewLabel.getHeight() * 2);
            	lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, lblNewLabel.getFont().getSize() * 2));
            }
        };
        
        
        mcerrar.addActionListener(a1);
        mmaximizar.addActionListener(a2);
        mrestaurar.addActionListener(a3);
		bpequeno.addActionListener(a4);
		bgrande.addActionListener(a5);
		
	}
}
