package views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class TornarJugar extends JFrame{

	private static JPanel contentPane;
	private static JButton aceptar = new JButton();
	private static JButton cancelar = new JButton();
	
	public TornarJugar() {
		setBounds(100, 100, 312, 196);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("¿Quieres empezar otra partida?");
		lblNewLabel.setBounds(59, 31, 187, 32);
		getContentPane().add(lblNewLabel);
		aceptar.setText("Aceptar");
		
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PrincipalVista.borrar_componentes();
				PrincipalVista.instanciarNiveles();
			}
		});
		aceptar.setBounds(29, 76, 97, 25);
		getContentPane().add(aceptar);
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		cancelar.setText("Cancelar");
		
		cancelar.setBounds(170, 76, 97, 25);
		getContentPane().add(cancelar);
	}
	

}
