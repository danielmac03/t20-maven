package views;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import views.PrincipalVista;

public class CombinacionVista extends JFrame{

	private JPanel contentPane;
	public ArrayList<JButton> coloresDisponiblesBtn = new ArrayList<JButton>();
	public ArrayList<Color> coloresCombinacion = new ArrayList<Color>();
	private int contadorBtns[] = {0, 0, 0, 0};

	public CombinacionVista(ArrayList<Color> coloresDisponibles) {
		setTitle("Mastermind - Escoger combinacion");
		setBounds(100, 100, 500, 200);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelBtns = new JPanel();
		panelBtns.setBounds(50, 50, 380, 50);
		contentPane.add(panelBtns);
		panelBtns.setLayout(new GridLayout(1, 4, 15, 50));
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(100, 111, 95, 28);
		btnAceptar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				obtenerCombinacionSecreta(coloresDisponibles);
				PrincipalVista.obtenerCombinacionSecreta(coloresCombinacion);
				dispose();
			}
		});
		contentPane.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(300, 111, 95, 28);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
		});
		contentPane.add(btnCancelar);
				
		MouseListener mlCombinacion = new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				JButton tmpBtn = (JButton) e.getSource();	
				
				for (int j = 0; j < coloresDisponibles.size(); j++) {
					if (coloresDisponiblesBtn.get(j) == tmpBtn) {	
						if (e.getButton() == MouseEvent.BUTTON1) {
							tmpBtn.setBackground(coloresDisponibles.get(contadorBtns[j]));
							if (contadorBtns[j]+1 < coloresDisponibles.size()) {
								contadorBtns[j]++;
							}
						}else if(e.getButton() == MouseEvent.BUTTON3) {
							if (contadorBtns[j]-1 >= 0) {
								contadorBtns[j]--;
							}
							tmpBtn.setBackground(coloresDisponibles.get(contadorBtns[j]));
						}
						break;
					}
				}

			}
		};
		
		for (int i = 0; i < 4; i++) {		
			coloresDisponiblesBtn.add(new JButton());
			coloresDisponiblesBtn.get(i).setBackground(new Color (255, 255, 255));
			coloresDisponiblesBtn.get(i).setPreferredSize(new Dimension(30, 30));
			coloresDisponiblesBtn.get(i).setBorderPainted(false);
			panelBtns.add(coloresDisponiblesBtn.get(i));
			coloresDisponiblesBtn.get(i).addMouseListener(mlCombinacion);
		}
		
	}
	
	public void obtenerCombinacionSecreta(ArrayList<Color> coloresDisponibles) {
		for (int i = 0; i < 4; i++) {
			coloresCombinacion.add(coloresDisponiblesBtn.get(i).getBackground());
		}
	}
}
