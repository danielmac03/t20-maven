package views;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import views.PrincipalVista;

public class ColoresVista extends JFrame{

	private JPanel contentPane;
	public ArrayList<JButton> coloresDisponiblesBtn = new ArrayList<JButton>();
	public ArrayList<Color> coloresDisponibles = new ArrayList<Color>();
	
	public ColoresVista(int colores) {
		setTitle("Mastermind - Escoger colores");
		setBounds(100, 100, 500, 200);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelBtns = new JPanel();
		panelBtns.setBounds(50, 50, 380, 50);
		contentPane.add(panelBtns);
		panelBtns.setLayout(new GridLayout(1, 4, 15, 50));
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(100, 111, 95, 28);
		btnAceptar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				obtenerColores();
				PrincipalVista.aplicarParametrosColores(colores, coloresDisponibles);
				dispose();
			}
		});
		contentPane.add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(300, 111, 95, 28);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
		});
		contentPane.add(btnCancelar);
				
		ActionListener alEscogerColores = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton tmpBtn = (JButton) e.getSource();
				
				Color color = JColorChooser.showDialog(null, "Seleccione un color:", new Color (255, 255, 255));
				
				if (color != null) {
					tmpBtn.setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue()));
				}
				
			}
		};
		
		for (int i = 0; i < colores; i++) {		
			coloresDisponiblesBtn.add(new JButton());
			coloresDisponiblesBtn.get(i).setBackground(new Color (255, 255, 255));
			coloresDisponiblesBtn.get(i).setPreferredSize(new Dimension(30, 30));
			coloresDisponiblesBtn.get(i).setBorderPainted(false);
			panelBtns.add(coloresDisponiblesBtn.get(i));
			coloresDisponiblesBtn.get(i).addActionListener(alEscogerColores);
		}
		
	}
	
	public void obtenerColores() {
		for (int i = 0; i < coloresDisponiblesBtn.size(); i++) {
			coloresDisponibles.add(coloresDisponiblesBtn.get(i).getBackground());
		}
	}
}
