package views;

import java.awt.event.*;
import javax.swing.*;
import views.PrincipalVista;

public class NivelesVista extends JFrame{
	
	private JPanel contentPane;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private int intentos = 10;
	private int colores = 4;

	public NivelesVista(){		
		setTitle("Mastermind - Escoger nivel");
		setBounds(100, 100, 300, 225);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton principiante = new JRadioButton("Principiante");
		buttonGroup.add(principiante);
		principiante.setBounds(80, 30, 100, 30);
		contentPane.add(principiante);
		
		JRadioButton medio = new JRadioButton("Medio");
		buttonGroup.add(medio);
		medio.setBounds(80, 60, 100, 30);
		contentPane.add(medio);
		
		JRadioButton avanzado = new JRadioButton("Avanzado");
		buttonGroup.add(avanzado);
		avanzado.setBounds(80, 90, 100, 30);
		contentPane.add(avanzado);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (medio.isSelected()) {
					intentos = 8;
					colores =  5;
				} else if (avanzado.isSelected()) {
					intentos = 6;
					colores = 6;
				}
						
				PrincipalVista.aplicarParametrosNiveles(intentos, colores);
				
				dispose();
			}
		});
		btnNewButton.setBounds(30, 140, 97, 25);
		contentPane.add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(155, 140, 97, 25);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub				
				PrincipalVista.aplicarParametrosNiveles(intentos, colores);
				
				dispose();
			}
		});
		
		contentPane.add(btnCancelar);
	}

}
