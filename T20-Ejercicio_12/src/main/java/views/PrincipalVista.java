package views;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import views.*;

public class PrincipalVista extends JFrame{

	private JPanel contentPane;
	private static JPanel panelColoresDisponibles;
	private static int intentos;
	private static int colores;
	private int intentosActuales = 0;
	private static JButton[][] probarCombinacionBtn = new JButton[11][4];
	private static JButton combinacionSecreta[] = new JButton[4];
	private static ArrayList<JButton> coloresDisponiblesBtn = new ArrayList<JButton>();
	private static ArrayList<Color> coloresDisponibles = new ArrayList<Color>();
	private int contadorBtns[][] = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0} ,{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
	private static boolean jugar = false;
	private static JPanel panelCombinacionSecreta;
	private static JPanel panelPistas[] = new JPanel[11];
	private static JPanel panelProbarCombinacion[] = new JPanel[11];
	private int valorYPanelPistas = 45;
	private JButton	btnComprobar = new JButton("Comprobar");
	
	public PrincipalVista() {
		setTitle("Mastermind");
		setBounds(100, 100, 1000, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 984, 26);
		contentPane.add(menuBar);

		JMenu menuArchivo= new JMenu("Archivo");
		menuBar.add(menuArchivo);
		
		JMenuItem subMenuNuevoJuego = new JMenuItem("Nuevo Juego");
		subMenuNuevoJuego.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				instanciarNiveles();
				
			}
		});
		menuArchivo.add(subMenuNuevoJuego);
		
		JMenuItem subMenuNivel = new JMenuItem("Nivel");
		subMenuNivel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				instanciarNiveles();
			}
		});
		menuArchivo.add(subMenuNivel);
		
		JMenuItem subMenuSalir = new JMenuItem("Salir");
		subMenuSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tornarJugar();
			}
		});
		menuArchivo.add(subMenuSalir);
		
		JMenu menuAyuda = new JMenu("Ayuda");
		menuBar.add(menuAyuda);
		
		generarPanelProbarCombinacion();
		generarPanelPistas();		
		valorYPanelPistas += 45;
				
		panelColoresDisponibles = new JPanel();
		panelColoresDisponibles.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Colores disponibles", TitledBorder.LEADING, TitledBorder.TOP, null, SystemColor.textHighlight));
		panelColoresDisponibles.setBounds(705, 50, 237, 99);
		contentPane.add(panelColoresDisponibles);
		panelColoresDisponibles.setLayout(new GridLayout(1, 4, 15, 0));

		panelCombinacionSecreta = new JPanel();
		panelCombinacionSecreta.setBorder(new TitledBorder(null, "Combinaci\u00F3n secreta", TitledBorder.LEADING, TitledBorder.TOP, null, SystemColor.textHighlight));
		panelCombinacionSecreta.setBounds(700, 175, 242, 92);
		contentPane.add(panelCombinacionSecreta);
		panelCombinacionSecreta.setLayout(new GridLayout(1, 4, 15, 0));
				
		btnComprobar.setBounds(220, 54, 100, 23);
		btnComprobar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub			
				if (jugar == true) {
					int contador = 0;
					boolean intentoComprobar = false;					
					
					for (int j = 0; j < 4; j++) {
						JButton tmpBtn = new JButton();
						tmpBtn.setBorderPainted(false);
						
						panelPistas[intentosActuales].add(tmpBtn);
						
						panelPistas[intentosActuales].validate();
						panelPistas[intentosActuales].repaint();
												
						if (probarCombinacionBtn[intentosActuales][j].getBackground() == combinacionSecreta[j].getBackground()) {
							contador++;
							tmpBtn.setBackground(new Color(0, 0, 0));
						}else {
							tmpBtn.setBackground(new Color(255, 255, 255));
							intentoComprobar = true;
						}
					}
					
					if (intentoComprobar = true) {
						intentosActuales++;
						if (intentosActuales == intentos) {
							jugar = false;
							mostrarSolucion();
							JOptionPane.showMessageDialog(null, "Has perdido");
							tornarJugar();
						}
					}
					
					if (contador == 4) {
						jugar = false;
						mostrarSolucion();
						JOptionPane.showMessageDialog(null, "Ganaste");
						tornarJugar();
					}
					
					generarPanelProbarCombinacion();
					generarPanelPistas();
					valorYPanelPistas+=45;
					contentPane.validate();
					contentPane.repaint();
				}
				
			}
		});
		contentPane.add(btnComprobar);
		
		JMenuItem subMenuComoJugar = new JMenuItem("Como Jugar");
		subMenuComoJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,"Mastermind es un juego de habilidad y lógica que consiste en descubrir una secuencia de colores oculta."
						+ " El primer paso es escoger el nivel de dificultad del juego:" + "\n" + "- Nivel Principiante: 4 colores posibles y 10 intentos"
						+ "\n" + "- Nivel Medio: 5 colores posibles y 8 intentos"+ "\n" + "- Nivel Avanzado: 6 colores posibles y 6 intentos" + "\n" 
						+ "Una vez escogido el nivel, uno de los jugadores creará una combinación de cuatro colores utilizando únicamente los colores posibles."
						+ "El código de colores debe" + "\n" + "ocultarse para que el otro jugador lo acierte con un máximo de intentos predeterminados." 
						+ " Para decifrarlo, éste deberá ir probando combinaciones y el jugador" + "\n" + "contrario ir dándole pistas. Las pistas consisten en que "
						+ "si se ha acertado un color pero no su posición, se mostrará el color blanco, y si se ha acertado el color "
						+ "\n" + "i la posición, el color negro. El juego termina cuando el jugador que decifra acierta el código o cuando se le acaban los intentos. "); 
			}
		});
		menuAyuda.add(subMenuComoJugar);
		
		JMenuItem subMenuAcercaDe = new JMenuItem("Acerca De");
		subMenuAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,"Creado por Arnau Colomé, Alba Fischer y Daniel Macías");
			}
		});
		menuAyuda.add(subMenuAcercaDe);
		
		
		for (int i = 0; i < 4; i++) {			
			JButton tmpBtn = new JButton();
			tmpBtn.setBackground(new Color (255, 255, 255));
			tmpBtn.setBorderPainted(false);
		}
	}
	
	public static void instanciarNiveles() {
		NivelesVista nivelesVista = new NivelesVista();
		nivelesVista.setVisible(true);
	}
	
	public static void aplicarParametrosNiveles(int intentos_nivelVista, int colores_nivelVista) {
		intentos = intentos_nivelVista;
		colores = colores_nivelVista;
		instanciarColores();
	}
	
	public static void instanciarColores() {
		ColoresVista coloresVista = new ColoresVista(colores);
		coloresVista.setVisible(true);
	}
	
	public static void aplicarParametrosColores(int colores, ArrayList<Color> coloresDisponibles_coloresVista) {		
		coloresDisponibles = coloresDisponibles_coloresVista;
		
		for (int i = 0; i < colores; i++) {		
			coloresDisponiblesBtn.add(new JButton());
			coloresDisponiblesBtn.get(i).setBackground(coloresDisponibles_coloresVista.get(i));
			coloresDisponiblesBtn.get(i).setBorderPainted(false);
			panelColoresDisponibles.add(coloresDisponiblesBtn.get(i));
		}
		
		jugar = true;
		
		panelColoresDisponibles.validate();
		panelColoresDisponibles.repaint();		
		
		instanciarCombinacion(coloresDisponibles_coloresVista);
	}
	
	public static void instanciarCombinacion(ArrayList<Color> coloresDisponibles) {
		CombinacionVista combinacionVista = new CombinacionVista(coloresDisponibles);
		combinacionVista.setVisible(true);
	}
	
	public static void obtenerCombinacionSecreta(ArrayList<Color> combinacion_coloresVista) {		
		
		for (int i = 0; i < 4; i++) {		
			combinacionSecreta[i] = new JButton();
			combinacionSecreta[i].setBackground(combinacion_coloresVista.get(i));
			combinacionSecreta[i].setBorderPainted(false);
			combinacionSecreta[i].setVisible(false);
			panelCombinacionSecreta.add(combinacionSecreta[i]);
		}
		
		jugar = true;
		
		panelColoresDisponibles.validate();
		panelColoresDisponibles.repaint();
	}
	
	public static void mostrarSolucion() {
		for (int i = 0; i < 4; i++) {		
			combinacionSecreta[i].setVisible(true);
		}
	}
	
	public static void borrar_componentes() {
		PrincipalVista view = new PrincipalVista();
		view.setVisible(true);    
	}
	
	public void tornarJugar() {
		TornarJugar tornarJugar = new TornarJugar();
		tornarJugar.setVisible(true);
		dispose();				
	}
	
	public void generarPanelProbarCombinacion(){
		panelProbarCombinacion[intentosActuales] = new JPanel();
		panelProbarCombinacion[intentosActuales].setBounds(30, valorYPanelPistas, 180, 30);
		contentPane.add(panelProbarCombinacion[intentosActuales]);
		panelProbarCombinacion[intentosActuales].setLayout(new GridLayout(1, 4, 15, 0));
		
		for (int i = 0; i < 4; i++) {		
			probarCombinacionBtn[intentosActuales][i] = new JButton();
			probarCombinacionBtn[intentosActuales][i].setBackground(new Color(255, 255, 255));
			probarCombinacionBtn[intentosActuales][i].setBorderPainted(false);
			probarCombinacionBtn[intentosActuales][i].addMouseListener(new MouseListener() {				
				@Override
				public void mouseReleased(MouseEvent e) {}
				
				@Override
				public void mousePressed(MouseEvent e) {}
				
				@Override
				public void mouseExited(MouseEvent e) {}
				
				@Override
				public void mouseEntered(MouseEvent e) {}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					if (jugar == true) {
						JButton tmpBtn = (JButton) e.getSource();
						
						for (int j = 0; j < 4; j++) {
							if (probarCombinacionBtn[intentosActuales][j] == tmpBtn) {	
								if (e.getButton() == MouseEvent.BUTTON1) {
									tmpBtn.setBackground(coloresDisponiblesBtn.get(contadorBtns[intentosActuales][j]).getBackground());
									if (contadorBtns[intentosActuales][j]+1 < colores) {
										contadorBtns[intentosActuales][j]++;
									}
								}else if(e.getButton() == MouseEvent.BUTTON3) {
									if (contadorBtns[intentosActuales][j]-1 >= 0) {
										contadorBtns[intentosActuales][j]--;
									}
									tmpBtn.setBackground(coloresDisponiblesBtn.get(contadorBtns[intentosActuales][j]).getBackground());
								}
								break;
							}
						}
					}
				}
				
			});
			
			panelProbarCombinacion[intentosActuales].add(probarCombinacionBtn[intentosActuales][i]);
			btnComprobar.setBounds(220, valorYPanelPistas+9, 100, 23);
		}
	}
	
	public void generarPanelPistas() {
		panelPistas[intentosActuales] = new JPanel();
		panelPistas[intentosActuales].setBounds(361, valorYPanelPistas, 300, 40);
		contentPane.add(panelPistas[intentosActuales]);
		panelPistas[intentosActuales].setLayout(new GridLayout(1, 4, 15, 0));
		
		for (int i = 0; i < 4; i++) {
			probarCombinacionBtn[intentosActuales][i].setBackground(new Color(255, 255, 255));
			panelProbarCombinacion[intentosActuales].add(probarCombinacionBtn[intentosActuales][i]);
		}
	}

}