package UD20.T20_Ejercicio_12;

import views.PrincipalVista;
import views.TornarJugar;

/**
 * Iniciamos la aplicación.
 */
public class App {
    
	public static void main(String[] args) {
		PrincipalVista view = new PrincipalVista();
		view.setVisible(true);   
	
	}

}
