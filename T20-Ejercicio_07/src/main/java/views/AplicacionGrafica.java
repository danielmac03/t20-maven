package views;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class AplicacionGrafica extends JFrame{

	private JPanel contentPane;

	public AplicacionGrafica() {
				
		setTitle("Ejercicio 08");
		setBounds(400, 200, 500, 180);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		final JLabel labels[] = new JLabel[3];
		final JTextField textFields[] = new JTextField[2];
		
		labels[0] = new JLabel("Cantidad a convertir:");
		labels[0].setBounds(50, 25, 150, 20);
		contentPane.add(labels[0]);
		
		textFields[0] = new JTextField();
		textFields[0].setBounds(180, 25, 100, 20);
		contentPane.add(textFields[0]);
		
		labels[1] = new JLabel("Resultado:");
		labels[1].setBounds(300, 25, 100, 20);
		contentPane.add(labels[1]);
		
		textFields[1] = new JTextField();
		textFields[1].setBounds(380, 25, 90, 20);
		textFields[1].setEnabled(false);
		contentPane.add(textFields[1]);
		
		JButton btns[] = new JButton[3];

		btns[1] = new JButton("Euros a pesetas");
		btns[1].setBounds(65, 60, 150, 25);
		contentPane.add(btns[1]);
		
		btns[0] = new JButton("Cambiar");
		btns[0].setBounds(215, 60, 150, 25);
		contentPane.add(btns[0]);

		
		labels[2] = new JLabel("");
		labels[2].setBounds(200, 100, 150, 20);
		contentPane.add(labels[2]);
	
		
		ActionListener alEurosPesetas = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String cantidadConvertir = textFields[0].getText();
					double cantidadConvertirDouble = Double.parseDouble(cantidadConvertir);
					double cantidadEuros = Math.floor((cantidadConvertirDouble * 166.386) * 100) / 100;
					String cantidadEurosString = Double.toString(cantidadEuros);
					textFields[1].setText(cantidadEurosString);
					labels[2].setText("EUROS A PESETAS");
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "Debe introducir un numero");
					textFields[0].setText("");
					textFields[1].setText("");
					labels[2].setText("");
				}
			}
		};
		btns[1].addActionListener(alEurosPesetas);
		
		ActionListener alPtasEuros = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String cantidadConvertir = textFields[0].getText();
					double cantidadConvertirDouble = Double.parseDouble(cantidadConvertir);
					double cantidadPesetas = Math.floor((cantidadConvertirDouble / 166.386) * 100) / 100;
					String cantidadPesetasString = Double.toString(cantidadPesetas);
					textFields[1].setText(cantidadPesetasString);
					labels[2].setText("PESETAS A EUROS");
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "Debe introducir un numero");
					textFields[0].setText("");
					textFields[1].setText("");
					labels[2].setText("");
				}
			}
		};
		btns[0].addActionListener(alPtasEuros);

		
		

	}
}
