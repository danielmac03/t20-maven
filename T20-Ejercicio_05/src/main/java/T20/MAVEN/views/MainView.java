package T20.MAVEN.views;

import javax.swing.*;
import java.awt.event.*;

public class MainView extends JFrame{
	
	private JPanel contentPane;
	
	public MainView() {
		setTitle("Ejercicio 05");
		setBounds(400, 200, 800, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
				
		JButton btn = new JButton("Limpiar");
		btn.setBounds(350, 20, 80, 20);
		contentPane.add(btn);
		
		final JTextArea textarea = new JTextArea("");
		textarea.setBounds(0, 80, 800, 400);
		contentPane.add(textarea);
		
		MouseListener ml = new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textarea.append("Ha pinchado y soltado\n");				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				textarea.append("Ha presionado el boton\n");				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				textarea.append("Ha soltado el boton\n");				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				textarea.append("Ha entrado\n");				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				textarea.append("Ha salido\n");				
			}
		};

		textarea.addMouseListener(ml);
		
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textarea.setText("");
			}
		};
		
		btn.addActionListener(al);

	}

}