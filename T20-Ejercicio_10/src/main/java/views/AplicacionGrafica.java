package views;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.*;

public class AplicacionGrafica extends JFrame {

	//Definir atributos necesarios
	private JPanel contentPane;
	//Indicar a quien le toca
	private int turno;
	//Permitor o no hacer clic en alg�n boton
	private boolean fin = true;
	//Todas la combinaciones ganadoras posibles
	private int[][] combGanadora = new int[][] { {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6} }; 

	//Definir atributos arrays de cada elemento
	JLabel labels[] = new JLabel[9];
	JTextField textFields[] = new JTextField[2];
	JRadioButton radioButtons[] = new JRadioButton[4];
	ButtonGroup radioButtonsGroup[] = new ButtonGroup[2];
	JButton btnsSettings[] = new JButton[2];
	JButton btnsGrid[] = new JButton[9];
	
	public AplicacionGrafica() {
										
		setTitle("Tres en raya");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 474);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
				
		btnsSettings[0] = new JButton("Empezar");
		btnsSettings[0].setBounds(450, 45, 100, 23);
		contentPane.add(btnsSettings[0]);
		
		btnsSettings[1] = new JButton("Nueva Partida");
		btnsSettings[1].setBounds(600, 45, 150, 23);
		contentPane.add(btnsSettings[1]);
				
		labels[0] = new JLabel("");
		labels[0].setFont(new Font("Tahoma", Font.PLAIN, 13));
		labels[0].setBounds(448, 84, 236, 14);
		contentPane.add(labels[0]);
		
		labels[1] = new JLabel("Jugador 1");
		labels[1].setFont(new Font("Tahoma", Font.PLAIN, 13));
		labels[1].setBounds(448, 109, 80, 17);
		contentPane.add(labels[1]);
		
		labels[2] = new JLabel("Nombre:");
		labels[2].setBounds(448, 144, 57, 14);
		contentPane.add(labels[2]);
		
		textFields[0] = new JTextField();
		textFields[0].setBounds(526, 141, 86, 20);
		textFields[0].setText("Jugador 1");
		contentPane.add(textFields[0]);
		textFields[0].setColumns(10);
		
		labels[3] = new JLabel("Tipo:");
		labels[3].setBounds(448, 181, 46, 14);
		contentPane.add(labels[3]);
		
		radioButtons[0] = new JRadioButton("Humano");
		radioButtons[0].setSelected(true);
		radioButtons[0].setBounds(500, 177, 74, 23);
		contentPane.add(radioButtons[0]);
		
		radioButtons[1] = new JRadioButton("CPU");
		radioButtons[1].setBounds(580, 177, 74, 23);
		contentPane.add(radioButtons[1]);
		
		radioButtonsGroup[1] = new ButtonGroup();
		radioButtonsGroup[1].add(radioButtons[0]);
		radioButtonsGroup[1].add(radioButtons[1]);
		
		labels[4] = new JLabel("Jugador 2");
		labels[4].setFont(new Font("Tahoma", Font.PLAIN, 13));
		labels[4].setBounds(448, 246, 80, 17);
		contentPane.add(labels[4]);
		
		labels[5] = new JLabel("Nombre:");
		labels[5].setFont(new Font("Tahoma", Font.PLAIN, 13));
		labels[5].setBounds(448, 277, 57, 14);
		contentPane.add(labels[5]);
		
		textFields[1] = new JTextField();		
		textFields[1].setBounds(526, 274, 86, 20);
		textFields[1].setText("Jugador 2");
		textFields[1].setColumns(10);
		contentPane.add(textFields[1]);
		
		labels[6] = new JLabel("Tipo:");
		labels[6].setFont(new Font("Tahoma", Font.PLAIN, 13));
		labels[6].setBounds(448, 314, 46, 14);
		contentPane.add(labels[6]);
		
		radioButtons[2] = new JRadioButton("Humano");
		radioButtons[2].setSelected(true);
		radioButtons[2].setBounds(500, 310, 74, 23);
		contentPane.add(radioButtons[2]);
		
		radioButtons[3] = new JRadioButton("CPU");
		radioButtons[3].setBounds(580, 310, 66, 23);
		contentPane.add(radioButtons[3]);
		
		radioButtonsGroup[1] = new ButtonGroup();
		radioButtonsGroup[1].add(radioButtons[2]);
		radioButtonsGroup[1].add(radioButtons[3]);
		
		//Generar panel 3 x 3 
		JPanel panel = new JPanel();
		panel.setBounds(27, 44, 369, 346);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 3, 0, 0));
				
		//Generar botones con bucle
		for(int i=0; i<=8; i++)	{
			btnsGrid[i] = new JButton();
			btnsGrid[i].setFont(new Font("Tahoma", Font.PLAIN, 50));
			panel.add(btnsGrid[i]);
		}
		
		//Al hacer clic en el boton empezar se ejecutara lo siguiente
		ActionListener empezar = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				fin = false;
				
				//No se puede volver a hacer clic en empezar 
				btnsSettings[0].setEnabled(false);			
				
				//Cambiar el nombre de los jugadores por los nuevos
				labels[1].setText(textFields[0].getText());
				labels[4].setText(textFields[1].getText());
				
				//No se puede cambiar el nombre de nuevo
				textFields[0].setEnabled(false);
				textFields[1].setEnabled(false);
				
				
				//Mostrar mensaje de inicio
				labels[0].setText("La partida ha empezado!!");
				
//				comprobar(); 

				//Si le toca jugar primero a la CPU llamamos a la funcion control() para que haga su jugada
				if(radioButtons[1].isSelected()) {
					control();
				}

			}
		};
				
		//Asignar la accion al botno empezar
		btnsSettings[0].addActionListener(empezar);
			
		ActionListener nuevaPartida = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				AplicacionGrafica frame = new AplicacionGrafica();
				frame.setVisible(true);
			}
		};
		
		btnsSettings[1].addActionListener(nuevaPartida);
		
		
		//Al hacer clic en algun boton del tablero se ejecutara lo siguiente
		ActionListener selectButton = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton btn = (JButton) e.getSource(); 
				
				//Si el boton esta vacio y se puede jugar...
				if(btn.getText().equals("") && fin == false) {
					//Si es su turno y no juega la cpu....
					if (turno %2 == 0 && radioButtons[1].isSelected() == false) {
						//Asignamos a ese boton la X
						btn.setText("X");
						//Indicamos que es el turno del contrincante
						labels[0].setText(textFields[1].getText() + " es tu turno");
					}else if(turno %2 == 1 && radioButtons[3].isSelected() == false){
						//Asignamos a ese boton la O
						btn.setText("O");
						//Indicamos que es el turno del contrincante
						labels[0].setText(textFields[0].getText() + " es tu turno");
					}
					
					//Llamar a la funci�n control para comprobar si hay ganador o empate
					control();
				}
				
			}			
		};
		
		for(int i=0; i<=8; i++)	{		
			//Asignar accion a todos los botones del tablero
			btnsGrid[i].addActionListener(selectButton);
		}
		
	}
	
	public void control() {
		
		//Si es su turno y juega la cpu....
		if(turno %2 == 0 && radioButtons[1].isSelected()) {
			jugadaAutomatica();	
		}else if(turno %2 == 1 && radioButtons[3].isSelected()) {
			jugadaAutomatica();	
		}
		
		//Comprobar si hay ganador
		comprobarGanador();
		
		//Si no hay ganador...
		if(fin == false) {
			//Comprobar si hay empete
			comprobarEmpate();
			
			//Si no hay empete...
			if(fin == false) {
				//Sumar un turno
				turno++;
				
				//Si es su turno y juega la cpu llamar de nuevo a esta funcion
				if((turno %2 == 0 && radioButtons[1].isSelected()) || (turno %2 == 1 && radioButtons[3].isSelected())) {
					control();
				}	
			}
			
		}
	}
	
	public void jugadaAutomatica() {
		String letter = null;
		String otherLetter = null;
		String player = null;
		String otherPlayer = null;
		int index;
		int newIndex = 9;
		boolean btnIsNotEmpty = false;
		boolean jugadaRandom = true;
		
		//Generar numero aleatrorio de un boton del tablero hasta que no este ocupado
		do {
			Random r = new Random();
			index = r.nextInt(8); 
			if(btnsGrid[index].getText().equals("")) {
				btnIsNotEmpty = true;
			}
			
		} while (btnIsNotEmpty == false);
		
		//Si es su turno...
		if(turno %2 == 0) {
			letter = "X";
			otherLetter = "O";
			player = textFields[0].getText();
			otherPlayer = textFields[1].getText();
		}else if(turno %2 == 1) {
			letter = "O";
			otherLetter = "X";
			player = textFields[1].getText();
			otherPlayer = textFields[0].getText();
		}
		
		//Comprobar todas la combinaciones para encontrar una ganadora o una que no le permita ganar el contrincante 
		for (int i = 0; i < combGanadora.length; i++) {
			//Si la combinacion ganadora esta completa por dos en todas la posiciones....
			if (btnsGrid[combGanadora[i][0]].getText() == letter && btnsGrid[combGanadora[i][1]].getText() == letter) {
				//Si la ultima posicion para ganar no esta ocupada...
				if (btnsGrid[combGanadora[i][2]].getText().equals("")) {
					//La jugada que se ejecutara ganara
					index = combGanadora[i][2];
					//Cambiar el valor de existWin para mas adelante
					jugadaRandom = false;
					//Salir del for
					break;
				}
			}else if(btnsGrid[combGanadora[i][0]].getText() == letter && btnsGrid[combGanadora[i][2]].getText() == letter) {
				//Si la ultima posicion para ganar no esta ocupada...
				if (btnsGrid[combGanadora[i][1]].getText().equals("")) {
					//La jugada que se ejecutara ganara
					index = combGanadora[i][1];
					//Cambiar el valor de existWin para mas adelante
					jugadaRandom = false;
					//Salir del for
					break;
				}
			}else if(btnsGrid[combGanadora[i][1]].getText() == letter && btnsGrid[combGanadora[i][2]].getText() == letter) {
				//Si la ultima posicion para ganar no esta ocupada...
				if (btnsGrid[combGanadora[i][0]].getText().equals("")) {
					//La jugada que se ejecutara ganara
					index = combGanadora[i][0];
					//Cambiar el valor de existWin para mas adelante
					jugadaRandom = false;
					//Salir del for
					break;
				}
			}
			
			//Si la combinacion ganadora del enemigo esta completa por dos....
			if (btnsGrid[combGanadora[i][0]].getText() == otherLetter && btnsGrid[combGanadora[i][1]].getText() == otherLetter) {
				//Si la ultima posicion para ganar no esta ocupada...
				if (btnsGrid[combGanadora[i][2]].getText().equals("")) {
					//La jugada que se ejecutara no dejara ganar al contrincante
					newIndex = combGanadora[i][2];
					//Cambiar el valor de jugadaRandom para mas adelante
					jugadaRandom = false;
				}
			}
		}
		
		//Si no existe una jugada ganadora y hay combinacion ganadora del enemigo...
		if (jugadaRandom == true && newIndex != 9) {
			//Le asignamos a index la jugada que se ejecutara no dejara ganar al contrincante
			index = newIndex;
		}
		
		/*Ahora pulsamos el boton, el boton que se pulsara puede ser uno aleatorio por no encontrar ninguna combinacion,
		combinacion ganadora o anular la combinacion ganadora del contrincante*/
		btnsGrid[index].setText(letter);
		
		//Indicamos que es el turno del contrincante
		labels[0].setText(otherPlayer + " es tu turno");
	}
	
	public void comprobarGanador() {
		//Comprobar todas la combinaciones
		for (int i = 0; i < combGanadora.length; i++) {
			//Si hay una combinacion ganadora para alguno de los dos... 
			if(this.btnsGrid[combGanadora[i][0]].getText() == "X" && btnsGrid[combGanadora[i][1]].getText() == "X" && btnsGrid[combGanadora[i][2]].getText() == "X") {
				//Cambiar el texto que indicaba a quien le tocaba por FIN
				labels[0].setText("FIN");
				//Indicar quien gana
				JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
				//No permitir hacer clic en ningun boton
				fin = true;
				//Salir del for
				break;
			}else if(btnsGrid[combGanadora[i][0]].getText() == "O" && btnsGrid[combGanadora[i][1]].getText() == "O" && btnsGrid[combGanadora[i][2]].getText() == "O") {
				//Cambiar el texto que indicaba a quien le tocaba por FIN
				labels[0].setText("FIN");
				//Indicar quien gana
				JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
				//No permitir hacer clic en ningun boton
				fin = true;
				//Salir del for
				break;
			}	
		}
	}
	
	public void comprobarEmpate() {
		int completos = 0; 
		//Comprobar todos los botones
		for (int j = 0; j < btnsGrid.length; j++) {
			//Comprobar si esta vacio el boton
			boolean comprabarBtn= btnsGrid[j].getText().equals("");
			//Si no esta vacio...
			if(comprabarBtn == false) {
				//Aumentar el valor de completos
				completos++;
			}
		}
		
		//Si completos es igual a 9 es que no hay posiciones posible y como la funcion comprobarGanador se executa antes y no ha parado la ejecucion es que hay empete
		if(completos == 9) {
			//Cambiar el texto que indicaba a quien le tocaba por FIN
			labels[0].setText("FIN");
			//Mostrar que hay empete
			JOptionPane.showMessageDialog(null, "EMPATE");
			//No permitimos seguir jugando
			fin = true;
		}
	}
		
}
