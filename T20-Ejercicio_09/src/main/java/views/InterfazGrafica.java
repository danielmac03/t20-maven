package views;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class InterfazGrafica {

	public JFrame frmJuegoDeFormar;
	boolean girarCarta=false;
	boolean segonaCarta = false;
	Random random = new Random();
	Color color1,color2;
	Color [] colores = {Color.CYAN, Color.YELLOW, Color.BLUE, Color.RED, Color.PINK, Color.ORANGE,Color.GREEN, Color.WHITE,Color.CYAN, Color.YELLOW, Color.BLUE, Color.RED, Color.PINK, Color.ORANGE,Color.GREEN, Color.WHITE};
	ArrayList<Color> coloresArrayList = new ArrayList<>();
	JToggleButton boton1 = new JToggleButton();
	JToggleButton boton2 = new JToggleButton();

	int intentos=0, acierto=0;

	Hashtable<JToggleButton, Color> toggleButtons = new Hashtable<>();

	public InterfazGrafica() {
		frmJuegoDeFormar = new JFrame();
		frmJuegoDeFormar.setTitle("Juego de formar parejas");
		frmJuegoDeFormar.setBounds(100, 100, 470, 506);
		frmJuegoDeFormar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmJuegoDeFormar.getContentPane().setLayout(new GridLayout(0, 4, 0, 0));
		
		for (int i = 0; i < 16; i++) {
			coloresArrayList.add(colores[i]);
		}
		
		crearBotones();
	}

	
	private void crearBotones() {
		for (int i = 0; i < colores.length; i++) {
			JToggleButton toggleButtonsKey = new JToggleButton("");
			toggleButtonsKey.doClick(0);
			Color toggleButtonsValue = elegirColor();		
			ActionListener alCambiarColor = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					toggleButtonsKey.setBackground(toggleButtonsValue);
					habilitar(toggleButtonsKey);
				}
			};
			
			frmJuegoDeFormar.getContentPane().add(toggleButtonsKey);
			toggleButtonsKey.addActionListener(alCambiarColor);
			
			toggleButtonsKey.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseExited(MouseEvent e) {
					comparar();
				}
			});
			
			toggleButtons.put(toggleButtonsKey, toggleButtonsValue);
		}
	}
	
	private Color elegirColor () {	
		Color color = coloresArrayList.get((int) Math.floor(Math.random()*((coloresArrayList.size())-1-0+1)+0));
		coloresArrayList.remove(color);

		return color;
	}
	
	private void habilitar (JToggleButton button) {
		if (!girarCarta) {
			color1 = button.getBackground();
			girarCarta = true;	
			segonaCarta = false;
			boton1 = button;
		}else {
			button.setEnabled(true);
			color2 = button.getBackground();
			segonaCarta = true;
			girarCarta = false;	
			boton2 = button;
			
			if (color1.equals(color2)) {
				acierto++;
				acabar();
			}
			else {
				intentos++;
			}

		}

	}
	
	private void comparar () {
		if(segonaCarta) {		
			if (color1.equals(color2)) {
				boton1.setVisible(false);
				boton1.setEnabled(false);
				boton2.setVisible(false);
				boton2.setEnabled(false);
				
			}else {
				boton1.setSelected(true);
				boton2.setSelected(true);
				boton1.setEnabled(true);
				boton2.setEnabled(true);

			}
			girarCarta = false;

		}
		
	}
	
	public void acabar() {
		
		if (acierto == 8) {
			JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado en " + intentos + " intentos fallidos");
			
			frmJuegoDeFormar.getContentPane().setEnabled(false);
		}
		
	}
	
}



