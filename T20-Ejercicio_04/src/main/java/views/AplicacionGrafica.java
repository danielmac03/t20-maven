package views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.Window.Type;
import java.awt.event.*;

public class AplicacionGrafica extends JFrame{

	private JPanel contentPane;

	public AplicacionGrafica() {
				
		setTitle("Ejercicio 4");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 547, 724);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Eventos");
		lblNewLabel.setBounds(10, 83, 57, 34);
		contentPane.add(lblNewLabel);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(64, 11, 432, 218);
		contentPane.add(textArea);

			
		MouseListener m1 = new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textArea.append("Ha pinchado y soltado\n");				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				textArea.append("Ha presionado el boton\n");				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				textArea.append("Ha soltado el boton\n");				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				textArea.append("Ha entrado\n");				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				textArea.append("Ha salido\n");				
			}
			
		};
		
		KeyListener m2 = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				textArea.append("Tecla pulsada\n");	
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				textArea.append("Tecla Apretada\n");	
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				textArea.append("Tecla soltada\n");	
				
			}
			
		};
		
		textArea.addMouseListener(m1);
		textArea.addKeyListener(m2);
		
	}

}
